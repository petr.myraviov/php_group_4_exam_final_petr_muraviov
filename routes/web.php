<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::namespace('Admin')
    ->name('admin.')
    ->prefix('admin')
    ->middleware('auth')
    ->group(function () {
        Route::resource('articles','ArticlesController')->only('index', 'show', 'store');
        Route::resource('categories','CategorysController')->only('show');
        Route::resource('tags','TagController')->only('show');
        Route::resource('articles.comments', 'CommentsArticlesController')->only('store');
    });


Route::get('/', function () {
    return view('welcome');
});
Route::resource('news', 'PublicationController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
