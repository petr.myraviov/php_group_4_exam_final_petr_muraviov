<?php


namespace App\Filters;


class ArticleFilter extends QueryFilter
{
    /**
     * @param $value
     */
    public function category_id($value)
    {
        if (is_null($value)){
            $this->builder->where($value);
        } else {
            $this->builder->where('category_id','like', $value);
        }
    }
    /**
     * @param $value
     */
    public function tags($value)
    {
        if (is_null($value)) {
            $this->builder->where($value);
        } else {
            $this->builder->whereHas('tags', function ($query) use ($value) {
                $query->where('tag_id', $value);
            });
        }
    }
    /**
     * @return array
     */
    public function filters()
    {
        return $this->request->all();
    }
}
