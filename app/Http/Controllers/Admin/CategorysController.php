<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Tags;


class CategorysController extends Controller
{

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $tags = Tags::all();
        $categories= Category::all();
        $category = Category::findOrFail($id);
        $tag = Tags::findOrFail($id);
        return view('admin.categories.show', compact('tag', 'tags','category','categories'));
    }


}
