<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleRequest;
use App\Tags;

class ArticlesController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $tags = Tags::all();
        $articles = Article::all();
        $categories = Category::all();

        return view( 'admin.articles.index', compact('articles', 'tags', 'categories'));
    }


    /**
     * @param ArticleRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(ArticleRequest $request)
    {
        $article = new Article();
        $article->body = $request->input('body');
        $article->title = $request->input('title');
        $article->category_id = $request->input('category_id');
        $article->user_id = $request->user()->id;
        $article->save();

        $article->tags()->attach($request->input('tags'));

        return redirect(route('admin.articles.index'));
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $tags = Tags::all();
        $article = Article::findOrFail($id);
        $categories = Category::all();
        return view('admin.articles.show', compact('article', 'tags', 'categories'));
    }

}
