<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Tags;

class TagController extends Controller
{


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $tags = Tags::all();
        $tag = Tags::findOrFail($id);
        $categories= Category::all();
        return view('admin.tags.show', compact('tag', 'tags','categories'));
    }

}
