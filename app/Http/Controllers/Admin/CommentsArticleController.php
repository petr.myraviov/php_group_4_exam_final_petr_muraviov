<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Comment;
use App\Http\Controllers\Controller;
use App\Http\Requests\CommentRequest;
use Illuminate\Http\Request;

class CommentsArticleController extends Controller
{


    /**
     * @param CommentRequest $request
     * @param Article $article
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function store(CommentRequest $request, Article $article)
    {
        $comment = new Comment();
        $comment->body = $request->input('body');
        $comment->estimation =$request->input('estimation');
        $comment->article_id = $article->id;
        $comment->user_id = $request->user()->id;
        $comment->save();
        return response()->json([
            'comment' => view('components.comment', compact('comment'))->render()
        ], 201);
    }

}
