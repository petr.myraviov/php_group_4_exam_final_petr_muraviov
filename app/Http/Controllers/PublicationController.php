<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\Filters\ArticleFilter;
use App\Filters\CategoryFilters;
use App\Tags;
use Illuminate\Http\Request;

class PublicationController extends Controller
{

    /**
     * @param ArticleFilter $filters
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ArticleFilter $filters)
    {
        $tags = Tags::all();
        $articles = Article::with('tags')
            ->filter($filters)
            ->paginate(4);
        $categories = Category::all();

        return view( 'news.index', compact(
            'categories',
            'articles',
            'tags'));
    }

}
