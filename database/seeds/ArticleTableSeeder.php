<?php

use Illuminate\Database\Seeder;

class ArticleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $articles = factory(\App\Article::class,50)->create();
        foreach ($articles as $article)
        {
            $article->tags()->attach(range(rand(1,9),rand(1,9)));
        }
    }
}
