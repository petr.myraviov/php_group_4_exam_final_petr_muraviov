<?php

use Illuminate\Database\Seeder;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tags')->insert(
            [
                'tag_title' => 'Федор Кудряшов',
            ]);

        DB::table('tags')->insert(
            [
                'tag_title' => 'Москва',

            ]);

        DB::table('tags')->insert(
            [
                'tag_title' => 'Веб-студиям',

            ]);

        DB::table('tags')->insert(
            [
                'tag_title' => 'Браузер и его настройки',

            ]);

        DB::table('tags')->insert(
            [
                'tag_title' => 'Программное обеспечение',

            ]);

        DB::table('tags')->insert(
            [
                'tag_title' => 'Дополнительные услуги',

            ]);

        DB::table('tags')->insert(
            [
                'tag_title' => 'Типовые решения',

            ]);

        DB::table('tags')->insert(
            [
                'tag_title' => 'Интерфейс редактора
',

            ]);

        DB::table('tags')->insert(
            [
                'tag_title' => 'О системе управления',

            ]);
    }

}
