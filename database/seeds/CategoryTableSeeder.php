<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert(
            [
                'title_category' => 'Палитика',
            ]);

        DB::table('categories')->insert(
            [
                'title_category' => 'Спорт',
            ]);

        DB::table('categories')->insert(
            [
                'title_category' => 'Новости',
            ]);

        DB::table('categories')->insert(
            [
                'title_category' => 'Интернет',
            ]);

        DB::table('categories')->insert(
            [
                'title_category' => 'Религия',
            ]);

        DB::table('categories')->insert(
            [
                'title_category' => 'Общество',
            ]);

        DB::table('categories')->insert(
            [
                'title_category' => 'Происшествия‎',
            ]);

        DB::table('categories')->insert(
            [
                'title_category' => 'Наука и технологии',
            ]);

        DB::table('categories')->insert(
            [
                'title_category' => 'Экономика‎',
            ]);
    }
}
