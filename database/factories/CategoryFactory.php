<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Category::class, function (Faker $faker) {
    return [
        'title_category' => $faker->sentence($nbWords = 6, $variableNbWords = true),
    ];
});
