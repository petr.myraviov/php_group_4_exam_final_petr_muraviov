<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Comment::class, function (Faker $faker) {
    return [
        'body' => $faker->text,
        'user_id' => rand(1,5),
        'estimation' => rand(1,5),
        'article_id' => rand(1,5)
    ];
});
