<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Tags::class, function (Faker $faker) {
    return [
        'tag_title' => $faker->sentence($nbWords = 6, $variableNbWords = true),
    ];
});
