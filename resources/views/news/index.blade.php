@extends('layouts.app')
@section('content')
    <div class="container">
        <div class=" mt-2">
            <form action="{{route('news.index')}}" method="get">
                <div class="row d-flex justify-content-end mb-4">
                    <div class="form-group ml-1 mb-1">
                        <select class="form-control" name="tags">
                            <option value="">Все теги</option>
                            @foreach($tags as $tag)
                                <option value="{{$tag->id}}">{{$tag->tag_title}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group ml-1 mb-1">
                        <select class="form-control" name="category_id">
                            <option value="">Все категории</option>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->title_category}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group ml-1 mb-1">
                        <button  class="btn btn-danger" type="submit"> Искать

                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="row mt-5">
            <div class="col-8">
                <h2 class="mb-4">Article</h2>
                @foreach($articles->sortByDesc('created_at') as $article)
                    <h3 class="mb-2">{{$article->title}}</h3>
                    <h4 class="text">{{$article->category->title_category}}</h4>
                    <h5 class="text-secondary mb-4"><span class="mr-1">{{$article->created_at->format('d M Y')}}</span> /
                        {{$article->tags()->pluck('tag_title')->implode(', ')}}&nbsp;</h5>
                    <p class="text">{{$article->body}}</p>
                    <p>
                        <b>Автор стотьи:</b> {{$article->user->name}}
                    </p>
                    <p class="text-right"><a href="{{route('admin.articles.show', ['article' => $article])}}">read more...</a></p>
                    <hr class="mt-5 mb-5">
                @endforeach
            </div>

            </div>
        </div>
    </div>
    <div style="margin-top: 50px" class="col-md-13 offset-md-4">
        {{$articles->withQueryString()->links()}}
    </div>

@endsection
