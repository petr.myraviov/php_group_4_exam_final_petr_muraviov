@extends('layouts.app')

@section('content')
    <div class="container">
    <div class="row mt-5">
        <div class="col-8">
            <h2 class="mb-4">Article</h2>
            @foreach($tag->articles->sortByDesc('created_at') as $article)
                <h4>{{$article->category->title_category}}</h4>
                <h3 class="mb-2">{{$article->title}}</h3>
                <h5 class="text-secondary mb-4"><span class="mr-1">{{$article->created_at->format('d M Y')}}</span> /
                    {{$article->tags()->pluck('tag_title')->implode(', ')}}&nbsp;</h5>
                <p class="text">{{$article->body}}</p>
                <p>
                    <b>Автор стотьи:</b> {{$article->user->name}}
                </p>
                <p class="text-right"><a href="{{route('admin.articles.show', ['article' => $article])}}">read more...</a></p>
                <hr class="mt-5 mb-5">
            @endforeach
        </div>
        <div class="col-4">
            <a href="{{route('admin.articles.index')}}" class="btn btn-primary btn-sm mb-4">Add article</a>
            <h4>Tags</h4>
            @foreach($tags as $tag)
                <div><a href="{{route('admin.tags.show', ['tag' => $tag])}}">{{$tag->tag_title}}</a></div>
            @endforeach

            <h4 style="margin-top: 10px">Category</h4>
            @foreach($categories as $category)
                <div><a href="{{route('admin.categories.show', ['category' => $category])}}">{{$category->title_category}}</a></div>
            @endforeach
        </div>
    </div>
    </div>

@endsection
