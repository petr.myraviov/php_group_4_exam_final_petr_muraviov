@extends('layouts.app')

@section('content')
    <div class="container">
    <div class="row mt-5">
        <h1 class="col-12 mb-4">{{$article->title}}</h1>
        <div class="col-8">
            <h4>{{$article->category->title_category}}</h4>
            <h5 class="text-secondary mb-4"><span class="mr-1">{{$article->created_at->format('d M Y')}}</span> /
                {{$article->tags()->pluck('tag_title')->implode(', ')}}&nbsp;</h5>
            <h6>{{$article->body}}</h6>
            <p>
                <b>Автор стотьи:</b> {{$article->user->name}}
            </p>
        </div>
        <div class="col-4">
            <h4>Tags</h4>
            @foreach($tags as $tag)
                <div><a href="{{route('admin.tags.show', ['tag' => $tag])}}">{{$tag->tag_title}}</a></div>
            @endforeach

            <h4 style="margin-top: 10px">Category</h4>
            @foreach($categories as $category)
                <div><a href="{{route('admin.categories.show', ['category' => $category])}}">{{$category->title_category}}</a></div>
            @endforeach
        </div>
    </div>

    <div class="row">
        <div id="comments-block" class="col-8 scrollit">
            @foreach($article->comments as $comment)
                <x-comment :comment="$comment"></x-comment>
            @endforeach
            <div class="col-4">
                <div class="comment-form">
                    <form id="create-comment-{{$article->id}}">
                        @csrf
                        <input type="hidden" id="article_id-{{$article->id}}" value="{{$article->id}}">
                        <div class="form-group">
                            <label for="body">Комментарий</label>
                            <textarea name="body" class="form-control" id="body" rows="3" required></textarea>
                        </div>
                        <p><select id="estimation" name="estimation">
                                <option selected disabled>Выберите оценку</option>
                                <option value="{{1}}">Не очень</option>
                                <option value="{{-1}}">Удовлетворительно</option>
                            </select></p>
                        <button  id="create-comment-btn" data-article-id="{{$article->id}}" type="submit" class="btn btn-outline-primary btn-sm btn-block create-comment-btn">Добавьте новый
                            комментарий
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    @csrf
@endsection
