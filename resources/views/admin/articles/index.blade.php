@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row mt-5">
            <div class="col-8">

                <h2>Add new article</h2>
                <form action="{{route('admin.articles.store')}}" method="post">
                    @csrf
                    <div class="form-group">
                        <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" placeholder="Enter article title">
                        @error('title')

                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="row">
                            @if('required')
                                <div class="col-6">
                                    <div class="checkselect mb-4 mt-4">
                                        @foreach($tags as $tag)
                                            <br>
                                            <label><input type="checkbox"  name="tags[]" value="{{$tag->id}}">{{$tag->tag_title}}</label>
                                        @endforeach
                                    </div>
                                    @error('tags')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            @endif
                        </div>
                        <div class="row" style="order: -1;">
                            @if('required')
                                <div class="col-6">
                                    <div class="checkselect mb-4 mt-4">
                                        @foreach($categories as $category)
                                            <br>
                                            <label><input type="checkbox"  name="category_id" value="{{$category->id}}">{{$category->title_category}}</label>
                                        @endforeach
                                    </div>
                                    @error('category_id')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            @endif
                        </div>
                        <textarea class="form-control @error('body') is-invalid @enderror" name="body" type="text" placeholder="Enter article text" rows="5"></textarea>
                        @error('body')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
            <div class="col-4">
                <h4>Tags</h4>
                @foreach($tags as $tag)
                    <div><a href="{{route('admin.tags.show', ['tag' => $tag])}}">{{$tag->tag_title}}</a></div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
